#!/usr/bin/env python
# coding: utf-8

import os
import h5py
import argparse
import logging

import numpy as np
import scipy.signal as sig

import torch

from inspiral_sim import config

# Parse arguments from command line
def parse_cmd():
    parser = argparse.ArgumentParser()

    # preprocess arguments
    parser.add_argument('--fmin', type=float, default=config.DEFAULT_FMIN)
    parser.add_argument('--fmax', type=float, default=config.DEFAULT_FMAX)
    parser.add_argument('--alpha', type=float, default=config.DEFAULT_ALPHA)
    parser.add_argument('--time-roll', type=float, default=config.DEFAULT_TIME_ROLL)
    
    # input/output arguments
    parser.add_argument('--in-file', type=str)
    parser.add_argument('--out-dir', type=str)
    parser.add_argument('--is-signal', action='store_true')
    parser.add_argument('--log', type=str)

    params = parser.parse_args()
    return params

params = parse_cmd()

logging.basicConfig(filename=params.log, filemode='a', 
                    format='%(asctime)s - %(message)s', level=logging.DEBUG)

# Create output directory
os.makedirs(params.out_dir, exist_ok=True)


# phase filtering function
def phase_filter(data, fs, fmin, fmax, alpha=0.):
    ''' Phase filter 
    
    Parameters:
    - data
    - fs: sampling frequency in Hz
    - fmin, fmax: frequency band
    
    Returns:
    - data_filt: filtered data
    '''
    bb, ab = sig.butter(4, [fmin*2./fs, fmax*2./fs], btype='band')
    normalization = np.sqrt((fmax-fmin)/(fs/2))
    data_filt = sig.filtfilt(bb, ab, data)/normalization
    # apply smoothing window
    data_filt *= sig.tukey(data_filt.shape[1], alpha=alpha)  
    return data_filt

# Preprocessing
# open data file
logging.info('Reading input file from: {}'.format(params.in_file))
injection_parameters = {}
with h5py.File(params.in_file, 'r') as f:
    fs = f.attrs.get('fs', config.DEFAULT_SAMPLE_RATE)
    H1_data = f['H1/timeseries'][:]
    L1_data = f['L1/timeseries'][:]
    
    if params.is_signal:
        H1_snr = f['H1/snr'][:].ravel()
        L1_snr = f['L1/snr'][:].ravel()
        for key, val in f['injection_parameters'].items():
            injection_parameters[key] = val[:]
    
# roll L1 noise
if params.time_roll > 0. and (not params.is_signal):
    logging.info('Rolling LLO noise by {} seconds'.format(params.time_roll))
    L1_data = np.roll(L1_data, np.round(int(fs * params.time_roll)), axis=1)
    
# apply bandpass filter
logging.info('Applying phase filter: {} Hz to {} Hz'.format(params.fmin, params.fmax))
logging.info('Applying Tukey window with alpha: {}'.format(params.alpha))
H1_data = phase_filter(H1_data, fs, params.fmin, params.fmax, params.alpha)
L1_data = phase_filter(L1_data, fs, params.fmin, params.fmax, params.alpha)

# Save
data = np.stack([H1_data, L1_data], axis=1)
if params.is_signal:
    snr = np.stack([H1_snr, L1_snr], axis=1)
logging.info('Saving output to directory: {}'.format(params.out_dir))
for i in range(len(data)):
    savepath = os.path.join(params.out_dir, f'data_{i}.pt')
    
    # Create save object
    # write time series and label
    savedict = {
        'x': data[i],
        'y': [int(params.is_signal)]
    }
    # write SNR and injection parameters if signal
    if params.is_signal:
        savedict['snr'] = snr[i]
        for key, val in injection_parameters.items():
            savedict[key] = val[i]
        
    # write to file
    torch.save(savedict, savepath)
    
    
#     torch.save({
#         'x': data[i],
#         'y': [int(params.is_signal)]
#     }, savepath)
