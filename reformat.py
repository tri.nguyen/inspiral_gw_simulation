#!/usr/bin/env python
# coding: utf-8


import os
import shutil

data_dir = 'data/Aug2019_simp_HIGHZ_fixedsky/preprocessed_data'
label = 'O3_August'
num_noise = 10
num_signal = 10
out_dir = os.path.join(data_dir, label)

in_dirs = []
in_dirs += [os.path.join(data_dir, f'{label}_noise_{i}') for i in range(num_noise)]
in_dirs += [os.path.join(data_dir, f'{label}_signal_{i}') for i in range(num_signal)]

print('- Input directories:')
for in_dir in in_dirs:
    print(in_dir)

print('- Output directory: ')    
print(out_dir)

os.makedirs(out_dir)

counter = 0
for in_dir in in_dirs:
    for i in range(100000):
        in_fname = os.path.join(in_dir, 'data_{}.pt'.format(i))
        out_fname = os.path.join(out_dir, 'data_{}.pt'.format(counter))
        if not os.path.exists(in_fname):
            break 
        counter += 1
#         print(in_fname, out_fname)   
        shutil.move(in_fname, out_fname)
