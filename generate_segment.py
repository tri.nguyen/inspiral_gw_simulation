#!/usr/bin/env python
# coding: utf-8

import os
import argparse

import numpy as np

from gwpy.segments import DataQualityFlag


# Parse arguments from command line
def parse_cmd():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--gps-start',help='Start time of segments', type=int)
    parser.add_argument('--gps-end', help='End time of segments', type=int)
    parser.add_argument('--ifo', help='Inteferometer', default='H1L1')
    parser.add_argument('--segment-max-duration', help='Max duration of each segment', 
                        default=4096, type=int)
    parser.add_argument('--out-file', help='Output file', type=str)
    params = parser.parse_args()
    return params

params = parse_cmd()

if params.ifo == 'H1L1':
    H1_segments = DataQualityFlag.query(
        'H1:DMT-ANALYSIS_READY:1', params.gps_start, params.gps_end)
    L1_segments = DataQualityFlag.query(
        'L1:DMT-ANALYSIS_READY:1', params.gps_start, params.gps_end)
    segments = H1_segments & L1_segments
elif params.ifo == 'H1':
    segments = DataQualityFlag.query(
        'H1:DMT-ANALYSIS_READY:1', params.gps_start, params.gps_end)
elif params.ifo == 'L1':
    segments = DataQualityFlag.query(
        'L1:DMT-ANALYSIS_READY:1', params.gps_start, params.gps_end)

# Get segments
start, end, duration = [], [], []
for seg in segments.active:
    seg_start = np.arange(seg.start, seg.end, params.segment_max_duration)
    seg_end = seg_start + params.segment_max_duration
    seg_end[-1] = int(seg.end)
    
    start.append(seg_start)
    end.append(seg_end)
    duration.append(seg_end - seg_start)

start = np.concatenate(start)
end = np.concatenate(end)
duration = np.concatenate(duration)
    
# Save segments    
table = np.array([start, end, duration]).T
header = 'start     end     duration'
if params.out_file is None:
    params.out_file = f'{ifo}_segments.txt'
np.savetxt(params.out_file, table, header=header, fmt='%d')
