
import numpy as np
import scipy.interpolate

from . import config


def get_optimal_filter_snr(
    template_td, freq_psd, psd, fs=8192., fmin=20.):
    ''' Compute the optimal filter SNR of a given GW time-domain template 
    Integrate the frequency from fmin to Nqyust
    
    Parameters:
    - template_td: GW time-domain template of shape (N_template, length)
    - psd_file: ascii file containing PSD and frequencies
    - fs: sampling rate 
    - fmin: min frequency
    
    '''
    data = template_td.copy()
    if template_td.ndim == 1: 
        data = data.reshape(1, -1)
    N, L = data.shape
    
    # calculate FD strain
    data_fd = np.fft.rfft(data)/fs
    frequencies  = np.fft.rfftfreq(L)*fs
    dfreq = frequencies[1] - frequencies[0]
    
    # Get noise PSD from psd_file
    psd = scipy.interpolate.interp1d(
        freq_psd, psd, bounds_error=False, fill_value=0.)(frequencies)
    inv_psd = np.zeros_like(psd)
    inv_psd[psd > 0.0] = 1./psd[psd > 0.0]
    
    # SNR^2 = 4 \int_fmin^fmax abs(h)/PSD df
    snr = 4*np.abs(data_fd)**2*inv_psd*dfreq
    snr = np.sum(snr[:, fmin <= frequencies], 1)
    snr = np.sqrt(snr)
    
    if template_td.ndim == 1: 
        snr = float(snr)
    return snr


def get_optimal_filter_snr_from_file(
    template_td, psd_file, fs=8192, fmin=20):
    ''' Compute the optimal filter SNR of a given GW time-domain template 
    Integrate the frequency from fmin to Nqyust
    
    Parameters:
    - template_td: GW time-domain template of shape (N_template, length)
    - psd_file: ascii file containing PSD and frequencies
    - fs: sampling rate 
    - fmin: min frequency
    
    '''
    freq_psd, psd = np.genfromtxt(psd_file, unpack=True)
    snr = get_optimal_filter_snr(template_td, freq_psd, psd, fs=fs, fmin=fmin)
    return snr


def get_inspiral_range(
    template_td, freq_psd, psd, fs=8192, fmin=20., return_horizon=False):
    ''' Calculate the inspiral range of a given GW template 
    Horizon distance is defined as the maximum distance to a GW
    strain with SNR = 8
    
    Parameters:
    - template: GW template strain
    - freq, psd: frequency and power spectral density of noise
    - fs: sampling rate in Hz
    - fmin: minimum frequency 
    - return_horizon: return horizon distance instead
    
    Returns:
    -  d: average distance or horizon distance of GW template
    '''
    # Calculate matched filter SNR
    snr = get_optimal_filter_snr(template_td, freq_psd, psd, fs, fmin)
    
    # Calculate horizon and average distance
    d_hor = snr/8
    
    # convert horizon to avg distance
    F_avg = 2.2648  # NOTE: for BNS only
    d_avg = d_hor/F_avg
    
    if return_horizon:
        return d_hor
    return d_avg    


def get_inspiral_range_from_file(
    template_td, psd_file=config.aLIGOZeroDetHighPowerPSD, 
    fs=8192., fmin=20., return_horizon=False):
    ''' Calculate the inspiral range of a given GW template 
    Horizon distance is defined as the maximum distance to a GW
    strain with SNR = 8
    
    Parameters:
    - template: GW template strain
    - psd_file: ascii file containing PSD 
    - fs: sampling rate in Hz
    - fmin: minimum frequency 
    - return_horizon: return horizon distance instead
    
    Returns:
    -  d: average distance or horizon distance of GW template
    '''
    
    # Calculate matched filter SNR
    snr = get_optimal_filter_snr_from_file(template_td, psd_file, fs, fmin)
    
    # Calculate horizon and average distance
    d_hor = snr/8
    
    # convert horizon to avg distance
    F_avg = 2.2648  # NOTE: for BNS only
    d_avg = d_hor/F_avg
    
    if return_horizon:
        return d_hor
    return d_avg