
# All parameters
ALL_PARAMS_KEYS = (
    'noise_per_segment', 'num_segment', 'min_segment_len', 'fs', 'whiten_duration', 
    'duration', 'waveform_approximant', 'priors', 'min_frequency', 'reference_frequency', 
    'min_trigger_time', 'max_trigger_time', 'time_shift', 'out_file', 'segment_filename',
    'prefix', 'out_dir', 'num_noise', 'num_signal', 'job_name', 'accounting_group', 
    'notification', 'ifo', 'gps_start', 'gps_end', 'fmin', 'fmax', 'alpha', 'time_roll',
    'is_signal', 'in_file')

# PSD
aLIGOZeroDetHighPowerPSD = '/cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest/envs/ligo-py37/lib/python3.7/site-packages/bilby/gw/detector/noise_curves/aLIGO_ZERO_DET_high_P_psd.txt'

# Default parameters
# data properties
DEFAULT_SAMPLE_RATE = 2048
DEFAULT_WHITEN_DURATION = 16
DEFAULT_DURATION = 2

# segment properties
DEFAULT_NOISE_PER_SEGMENT = 1
DEFAULT_NUM_SEGMENT = 1
DEFAULT_MIN_SEGMENT_LEN = 256

# GW properties
DEFAULT_APPROXIMANT = 'IMRPhenomPv2'
DEFAULT_REFER_FREQ = 50
DEFAULT_MIN_FREQ = 20
DEFAULT_MIN_TRIGGER_TIME = 1.70
DEFAULT_MAX_TRIGGER_TIME = 1.80

# additional properties
DEFAULT_TIME_SHIFT = 0.

# preprocess proproties
DEFAULT_FMIN = 20
DEFAULT_FMAX = 1000
DEFAULT_ALPHA = 0.2
DEFAULT_TIME_ROLL = 0.

#     'event': {
#         'fs': int,
#         'fmin': float,
#         'fmax': float,
#         'alpha': float,
#         'duration':int,
#         'width': int,
#         'whiten_duration':int,
#         'time_shift': float,
#         'step_size': int,
#         'outfile': str,
#         'event_name': str,
#     }
