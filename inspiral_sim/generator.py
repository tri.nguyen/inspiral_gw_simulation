
import logging

logger = logging.getLogger(__name__)

import numpy as np 
import scipy.interpolate

import bilby
from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters

from . import config

def generate_gaussian_noise_td(
    n, psd_file=config.aLIGOZeroDetHighPowerPSD, 
    duration=1., fs=8192, fmin=20.):
    ''' Generate segments of gaussian noise given the PSD 
    
    Arguments:
    - n: number of segments to generate
    - psd_file: PSD file with frequency array and psd array
    - duration: time duration of each segment
    - fs: sampling rate
    - fmin: minimum frequency
    
    Returns:
    - noise_td: time-domain noise shape (n, fs*duration)
    
    '''
    # Get noise frequencies array
    dfreq = 1./duration
    nfreq = int(fs/2/dfreq) + 1
    frequencies = np.linspace(0., fs/2, nfreq)
    
    # Get noise PSD from psd_file
    freq_psd, psd = np.genfromtxt(psd_file, unpack=True)
    psd[freq_psd < fmin] = 0.
    psd = scipy.interpolate.interp1d(freq_psd, psd, bounds_error=False, fill_value=0.)(frequencies)
    
    # Generate gaussian noise
    sigma = np.sqrt(psd/4/dfreq)
    noise_fd = np.random.normal(0., sigma, size=(2, n, nfreq))
    noise_fd = noise_fd[0] + 1j*noise_fd[1]
    noise_td = np.fft.irfft(noise_fd)*fs
    
    return noise_td

def generate_inspiral_from_priors(n, priors, waveform_generator, ifo, duration):
    ''' Generate inspiral GW waveforms from priors and waveform generator 
    
    Arguments:
    - n: number of waveforms to generate
    - priors: Bilby priors to sample from
    - waveform_generator: Bilby waveform generator 
    - ifo: detector
    - duration: duration of time-domain signals
    
    Returns:
    - signals: time-domain signals
    '''
    parameters = priors.sample(n)
    signals, unsafe_indices = generate_inspiral_from_parameters(
        parameters, waveform_generator, ifo, duration)
    return parameters, signals, unsafe_indices


def generate_inspiral_from_parameters(parameters, waveform_generator, ifo, duration):
    ''' Generate inspiral from parameters 
    
    Arguments:
    - parameters: dictionary contain GW parameters
    '''
    
    if isinstance(ifo, str):
        ifo = bilby.gw.detector.get_empty_interferometer(ifo)
    
    # get waveform parameters 
    fs = waveform_generator.sampling_frequency
    generator_duration = waveform_generator.duration
    n_series = int(fs*generator_duration)
        
    # check if every parameter has the same length
    n = None
    for key, val in parameters.items():
        val_len = len(val)
        if n is None:
            n = val_len
        elif n != val_len:
            raise ValueError(f'key {key} has {val_len}, expected {n}')
    
    # start generate signals
    signals = np.zeros((n, int(duration*fs)))
    unsafe_indices = []  # record unsafe indices
    progress = 0
    for i in range(n):
        # print out checkpoint
        if n >= 10:
            if i % (n//10) == 0 : 
                logging.info(f'- Simulation progress - {progress*10}%')
                progress += 1
            
        # get parameter of a single injection
        params = dict()
        for key, val in parameters.items():
            params[key] = val[i]
        
        # check safe time with waveform generator
        parameters_check, _ = convert_to_lal_binary_black_hole_parameters(params)
        parameters_check = {key: parameters_check[key] for key in
                            ['mass_1', 'mass_2', 'a_1', 'a_2', 'tilt_1', 'tilt_2']}
        safe_time = bilby.gw.detector.get_safe_signal_duration(**parameters_check)
        if generator_duration < safe_time:
            logging.warning(f'duration {generator_duration} less than safe time {safe_time}')
            unsafe_indices.append(i)
            
        # get waveform polariazations
        waveform_polarizations = waveform_generator.time_domain_strain(params)
    
        # compute signals from polarization
        signal = np.zeros(n_series)
        for mode in waveform_polarizations.keys():
            det_response = ifo.antenna_response(
                params['ra'], params['dec'], params['geocent_time'],
                params['psi'], mode)
            signal += waveform_polarizations[mode] * det_response

        # time shift    
        time_shift = ifo.time_delay_from_geocenter(
            params['ra'], params['dec'], params['geocent_time'])
        dt = generator_duration/2 + time_shift
        signal = np.roll(signal, int(np.round(dt*fs)))  # time-shift
    
        # truncate signal
        idx_start = int(fs*(generator_duration - duration)/2)
        idx_stop = idx_start + int(fs*duration)
        signals[i] = signal[idx_start:idx_stop]
    
    return signals, unsafe_indices
