
import numpy as np

import scipy.signal as sig
import scipy.interpolate

def phase_filter(data, fs, fmin, fmax, alpha=0.):
    ''' Phase filter 
    
    Parameters:
    - data
    - fs: sampling frequency in Hz
    - fmin, fmax: frequency band
    
    Returns:
    - data_filt: filtered data
    '''
    bb, ab = sig.butter(4, [fmin*2./fs, fmax*2./fs], btype='band')
    normalization = np.sqrt((fmax-fmin)/(fs/2))
    data_filt = sig.filtfilt(bb, ab, data)/normalization
    # apply smoothing window
    data_filt *= sig.tukey(data_filt.shape[1], alpha=alpha)  
    return data_filt

def _fft_length_default(fs):
    """Choose an appropriate FFT length (in seconds) based on a sample rate
    Parameters
    ----------
    fs : `float`
        sampling rate in Hz
    Returns
    -------
    fftlength : `int`
        a choice of FFT length, in seconds
    """
    return int(max(2, np.ceil(2048 / fs)))


def whiten(data, fs, fftlength=None, overlap=0, window='hanning'):
    ''' Whitening data using ASD calculated by Welch's method
    
    Arguments:
    - data: array of shape (N, ) or (C, N)
    - fs: sample rate
    - fftlength: length of ASD (in seconds)
    '''
    
    if data.ndim == 1:
        nsample = len(data)
    else:
        nsample = data.shape[1]

    # Calculate ASD
    fftlength = fftlength if fftlength else _fft_length_default(fs)
    freq, psd = sig.welch(
        data, fs=fs, nperseg=fftlength*fs, 
        noverlap=overlap*fs, window=window)
    asd = np.sqrt(psd)

    # Interpolate ASD
    freq_interp = np.fft.rfftfreq(nsample) * fs
    asd = scipy.interpolate.interp1d(freq, asd)(freq_interp)

    # Calculate FFT of data
    data_fd = np.fft.rfft(data)

    # Whiten data by dividing the FFT by the ASD
    data_fd_whiten = data_fd * np.sqrt(2 / fs) / asd
    data_whiten = np.fft.irfft(data_fd_whiten)

    # apply tukey window to whitening data
    # the first and last 2 seconds of data will be corrupted
    alpha = 4 * fs / n
    data_whiten *= sig.tukey(nsample, alpha=alpha)
    
    return data_whiten
