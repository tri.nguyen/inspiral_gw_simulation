
import os
import sys
import argparse
import subprocess

import numpy as np

from pycondor import Job, Dagman

from inspiral_sim import io

# Set default parameters 
NOISE_PARAMS = (
    'noise_per_segment', 'num_segment', 'min_segment_len', 'fs', 'whiten_duration', 
    'duration', 'time_shift', 'segment_filename', 'log', 'out_file')
SIGNAL_PARAMS = (
    'noise_per_segment', 'num_segment', 'min_segment_len', 'fs', 'whiten_duration', 
    'duration', 'time_shift', 'segment_filename', 'priors', 'min_frequency',
    'reference_frequency', 'min_trigger_time', 'max_trigger_time', 'log',
    'waveform_approximant', 'out_file')
SEGMENT_PARAMS = ('gps_start', 'gps_end')
PREPROCESS_PARAMS = (
    'fmin', 'fmax', 'alpha', 'time_roll', 'is_signal', 'log')

def get_keys(data, keys):
    new = {}
    for k, v in data.items():
        if k in keys:
            new[k] = v
    return new

# Parse command line argument
def parse_cmd():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
    parser.add_argument('config', help='Path to config file', type=str) 
    parser.add_argument('--submit', help='Submit DAGMAN', action='store_true')
    params = parser.parse_args()
    return params

params = parse_cmd()

# Parse config file
config = io.parse_config(params.config, 'config')
out_dir = config['out_dir']
prefix = config['prefix']
job_name = config['job_name']
num_noise = int(config['num_noise'])
num_signal = int(config['num_signal'])

# Create output directory
os.makedirs(out_dir, exist_ok=True)

# Generate segment file
segment_filename = os.path.join(out_dir, 'segment.txt')
cmd = './generate_segment.py --gps-start {} --gps-end {} --out-file {}'.format(
    config['gps_start'], config['gps_end'], segment_filename)
subprocess.check_call(cmd.split(' '))


# Create a condor job for each noise and signal
# default condor
submit = 'condor/submit'
log = 'condor/log'
error = 'condor/error'
output = 'condor/output'
getenv = True
universe = config.get('universe', 'vanilla')
accounting_group = config['accounting_group']
notification = config.get('notification')
extra_lines = (
    'accounting_group = {}'.format(accounting_group),
    'stream_output = True',
    'stream_error = True',
)

# create DAGMAN 
dagman = Dagman(name='dag_{}'.format(job_name), submit=submit)
executable_noise = './generate_noise.py'
executable_signal = './generate_signal.py'
executable_preprocess  = './preprocess_data.py'

# Condor job to generate and preprocess noise
for i in range(num_noise):
        
    data_file = os.path.join(out_dir, '{}_noise_{}.h5'.format(prefix, i))
    
    # generator job args
    generator_dict = get_keys(config, NOISE_PARAMS)
    generator_dict['out_file'] = data_file
    generator_dict['segment_filename'] = segment_filename
    generator_args = io.dict2args(generator_dict)
    
    # preprocess job args
    preprocess_dict = get_keys(config, PREPROCESS_PARAMS)
    preprocess_dict['in_file'] = data_file
    preprocess_dict['out_dir'] = os.path.join(
        out_dir, 'preprocessed_data/{}_noise_{}'.format(prefix, i))
    preprocess_args = io.dict2args(preprocess_dict)
    
    # generator job
    generator_job = Job(
        name='noise_{}'.format(job_name),
        executable=executable_noise,
        submit=submit,
        log=log, 
        error=error,
        output=output,
        dag=dagman,
        notification=notification,
        getenv=getenv,
        universe=universe,
        request_memory='8 GB',
        extra_lines=extra_lines,
        arguments=[generator_args]
    )

    # preprocess job
    preprocess_job = Job(
        name='preprocess_noise_{}'.format(job_name),
        executable=executable_preprocess,
        submit=submit,
        log=log,
        error=error,
        output=output,
        dag=dagman,
        notification=notification,
        getenv=getenv,
        universe=universe,
        request_memory='1 GB',
        extra_lines=extra_lines,
        arguments=[preprocess_args]
    )

    # inter-job dependencies
    generator_job.add_child(preprocess_job)
    
# Condor job to generate signal
for i in range(num_signal):
    
    data_file = os.path.join(out_dir, '{}_signal_{}.h5'.format(prefix, i))
    
    # generator job args
    generator_dict = get_keys(config, SIGNAL_PARAMS)
    generator_dict['out_file'] = data_file
    generator_dict['segment_filename'] = segment_filename
    generator_args = io.dict2args(generator_dict)
    
    # preprocess job args
    preprocess_dict = get_keys(config, PREPROCESS_PARAMS)
    preprocess_dict['in_file'] = data_file
    preprocess_dict['out_dir'] = os.path.join(
        out_dir, 'preprocessed_data/{}_signal_{}'.format(prefix, i))
    preprocess_args = io.dict2args(preprocess_dict)
    preprocess_args += ' --is-signal'
    
    # generator job
    generator_job = Job(
        name='signal_{}'.format(job_name),
        executable=executable_signal,
        submit=submit,
        log=log, 
        error=error,
        output=output,
        dag=dagman,
        notification=notification,
        getenv=getenv,
        universe=universe,
        request_memory = '8 GB',
        extra_lines=extra_lines,
        arguments=[generator_args]
    )
    
    # preprocess job
    preprocess_job = Job(
        name='preprocess_signal_{}'.format(job_name),
        executable=executable_preprocess,
        submit=submit,
        log=log,
        error=error,
        output=output,
        dag=dagman,
        notification=notification,
        getenv=getenv,
        universe=universe,
        request_memory='1 GB',
        extra_lines=extra_lines,
        arguments=[preprocess_args]
    )
    
    # inter-job dependencies
    generator_job.add_child(preprocess_job)

if params.submit:
    dagman.build_submit()
else:
    dagman.build()
    