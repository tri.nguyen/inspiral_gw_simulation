
import os
import argparse
import subprocess

from inspiral_sim import io

# Set default parameters 
PARAMS = (
    'noise_per_segment', 'num_segment', 'min_segment_len', 'fs', 'whiten_duration', 
    'duration', 'time_shift', 'out_file', 'segment_filename', 'log')

# Parse command line argument
def parse_cmd():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
    parser.add_argument('config', help='Path to config file', type=str) 
    params = parser.parse_args()
    return params

params = parse_cmd()
config = io.parse_config(params.config, 'config')

# Call training script
cmd = './generate_noise.py ' + io.dict2args(config, PARAMS)
print('Run cmd: ' + cmd)
print('--------------------------')
subprocess.check_call(cmd.split(' '))
