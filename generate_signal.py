#!/usr/bin/env python
# coding: utf-8

import os
import sys
import h5py
import argparse
import logging

import numpy as np
import scipy.signal as sig
import scipy.interpolate

import bilby
from bilby.gw.source import lal_binary_black_hole
from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters

import gwdatafind
from glue.lal import Cache
from gwpy.timeseries import TimeSeries

from inspiral_sim import config, io, generator, gw

def get_timeseries(ifo, start, end):
    
    if ifo == 'H1':
        urls = gwdatafind.find_urls('H', 'H1_HOFT_C00', start, end)
        cache = Cache.from_urls(urls)
        data = TimeSeries.read(cache, 'H1:GDS-CALIB_STRAIN', start=start, end=end, 
                               nproc=4, verbose=True)
    elif ifo == 'L1':
        urls = gwdatafind.find_urls('L', 'L1_HOFT_C00', start, end)
        cache = Cache.from_urls(urls)
        data = TimeSeries.read(cache, 'L1:GDS-CALIB_STRAIN', start=start, end=end, 
                               nproc=4, verbose=True)
    return data


# Parse arguments from command line
def parse_cmd():
    parser = argparse.ArgumentParser()
    
    # data arguments
    parser.add_argument('--fs', type=float, default=config.DEFAULT_SAMPLE_RATE)
    parser.add_argument('--duration', type=float, default=config.DEFAULT_DURATION)
    parser.add_argument('--whiten-duration', type=float, default=config.DEFAULT_WHITEN_DURATION)
    
    # segment arguments
    parser.add_argument('--segment-filename', type=str)
    parser.add_argument('--num-segment', type=int, default=config.DEFAULT_NUM_SEGMENT)
    parser.add_argument('--noise-per-segment', type=int, default=config.DEFAULT_NOISE_PER_SEGMENT)
    parser.add_argument('--min-segment-len', type=float, default=config.DEFAULT_MIN_SEGMENT_LEN)
    
    # gw arguments
    parser.add_argument('--priors', type=str)
    parser.add_argument('--waveform-approximant', type=str, default=config.DEFAULT_APPROXIMANT)
    parser.add_argument('--min-frequency', type=float, default=config.DEFAULT_MIN_FREQ)
    parser.add_argument('--reference-frequency', type=float, default=config.DEFAULT_REFER_FREQ)
    parser.add_argument('--min-trigger-time', type=float, default=config.DEFAULT_MIN_TRIGGER_TIME)
    parser.add_argument('--max-trigger-time', type=float, default=config.DEFAULT_MAX_TRIGGER_TIME)
    
    # input/output
    parser.add_argument('--out-file', type=str)
    parser.add_argument('--log', type=str)
    
    # additional arguments
    parser.add_argument('--time-shift', type=float, default=config.DEFAULT_TIME_SHIFT)

    params = parser.parse_args()
    return params

params = parse_cmd()

logging.basicConfig(filename=params.log, filemode='a', 
                    format='%(asctime)s - %(message)s', level=logging.DEBUG)

# Get GW priors
priors = bilby.gw.prior.BBHPriorDict(filename=params.priors)

# Create waveform generator
waveform_generator = bilby.gw.WaveformGenerator(
    duration=params.whiten_duration, sampling_frequency=params.fs,
    frequency_domain_source_model=lal_binary_black_hole,
    parameter_conversion=convert_to_lal_binary_black_hole_parameters,
    waveform_arguments={
        'waveform_approximant': params.waveform_approximant,
        'reference_frequency': params.reference_frequency,
        'minimum_frequency': params.min_frequency
    }
)

# Get segments
seg_start, seg_end, seg_duration = np.genfromtxt(
    params.segment_filename, unpack=True)

# Shuffle segments
n_seg = len(seg_start)
mask = np.random.permutation(n_seg)
seg_start = seg_start[mask]
seg_end = seg_end[mask]
seg_duration = seg_duration[mask]

# Add segments 
H1_data, L1_data = [], []
H1_gps_start, L1_gps_start = [], []
H1_snr, L1_snr = [], []
injection_parameters = []

n = 0
for i in range(n_seg):
    # Get GPS start
    frame_start = seg_start[i]
    frame_end = seg_end[i]
    frame_duration = seg_duration[i]
    if frame_duration < params.min_segment_len:
        print(frame_duration, params.min_segment_len)
        continue
        
    # Get time series data
    H1_series = get_timeseries('H1', frame_start, frame_end)
    L1_series = get_timeseries('L1', frame_start, frame_end)
#     H1_series = TimeSeries.get(
#         'H1:GDS-CALIB_STRAIN', start=frame_start, end=frame_end,
#         nproc=4, verbose=True, allow_tape=True)
#     L1_series = TimeSeries.get(
#         'L1:GDS-CALIB_STRAIN', start=frame_start, end=frame_end,
#         nproc=4, verbose=True, allow_tape=True)

    # Resample
    H1_series = H1_series.resample(params.fs)
    L1_series = L1_series.resample(params.fs)

    gps_start = np.random.uniform(
        frame_start, frame_end - params.time_shift - params.whiten_duration,
        size=params.noise_per_segment)

    for start in gps_start:
        # Get time series 
        # Hanford
        idx_start = int(params.fs * (start - frame_start))
        idx_end = idx_start + int(params.fs * params.whiten_duration)
        H1 = H1_series[idx_start:idx_end]
        # Livingston with time shift
        idx_start = idx_start + int(params.fs * params.time_shift)
        idx_end = idx_end + int(params.fs * params.time_shift)
        L1 = L1_series[idx_start:idx_end]
        
        # Get trigger time and geocent time
        trigger_time = np.random.uniform(params.min_trigger_time, params.max_trigger_time)
        geocent_time = start + (params.whiten_duration - params.duration) / 2. + trigger_time
        signal_gps_start = geocent_time - params.duration / 2.
        priors['geocent_time'] = geocent_time
        
        # Generate GW signals from priors
        parameters, H1_signal, _ = generator.generate_inspiral_from_priors(
            1, priors, waveform_generator, 'H1', params.duration)
        L1_signal, _ = generator.generate_inspiral_from_parameters(
            parameters, waveform_generator, 'L1', params.duration)
        injection_parameters.append(parameters)
    
        # Calculate optimal filter SNR
        H1_psd = H1.psd(4)
        snr = gw.get_optimal_filter_snr(
            template_td=H1_signal, freq_psd=H1_psd.frequencies.value,
            psd=H1_psd.value, fs=params.fs, fmin=params.min_frequency)
        H1_snr.append(snr)

        L1_psd = L1.psd(4)
        snr = gw.get_optimal_filter_snr(
            template_td=L1_signal, freq_psd=L1_psd.frequencies.value,
            psd=L1_psd.value, fs=params.fs, fmin=params.min_frequency)
        L1_snr.append(snr)
        
        # Inject signal to time series
        # Hanford
        idx_start = int(params.fs * (signal_gps_start - H1.t0.value))
        idx_end = idx_start + int(params.fs * params.duration)
        H1[idx_start:idx_end] += H1_signal.ravel() * H1.unit
        # Livingston with the time shift
        idx_start = int(params.fs * (signal_gps_start + params.time_shift - L1.t0.value))
        idx_end = idx_start + int(params.fs * params.duration)
        L1[idx_start:idx_end] += L1_signal.ravel() * L1.unit
        
        # Whiten
        H1_whiten = H1.whiten()
        L1_whiten = L1.whiten()
        
        # Take only the center of the time series
        idx_start = int(params.fs * (params.whiten_duration - params.duration) / 2.)
        idx_end = idx_start + int(params.fs * params.duration)
        H1_new = H1_whiten[idx_start:idx_end]
        L1_new = L1_whiten[idx_start:idx_end]
        
        # Add to list
        H1_data.append(H1_new.value)
        H1_gps_start.append(H1_new.t0.value)
        L1_data.append(L1_new.value)
        L1_gps_start.append(L1_new.t0.value)

    n += 1
    if n >= params.num_segment:
        break

# Convert to array
H1_data = np.stack(H1_data)
L1_data = np.stack(L1_data)
H1_snr = np.stack(H1_snr).ravel()
L1_snr = np.stack(L1_snr).ravel()
H1_gps_start = np.stack(H1_gps_start)
L1_gps_start = np.stack(L1_gps_start)

# reformat injection parameters
new_injection_parameters = {}
for key in injection_parameters[0].keys():
    temp_params = []
    for single_inj_parameters in injection_parameters:
        temp_params.append(single_inj_parameters[key][0])
    new_injection_parameters[key] = np.array(temp_params)
    
# Write in HDF5 format
logging.info('Saving output to {}'.format(params.out_file))
with h5py.File(params.out_file, 'w') as f:
    # write data
    H1_gr = f.create_group('H1')
    H1_gr.create_dataset('timeseries', data=H1_data, compression='gzip')
    H1_gr.create_dataset('gps_start', data=H1_gps_start, compression='gzip')
    H1_gr.create_dataset('snr', data=H1_snr, compression='gzip')
    L1_gr = f.create_group('L1')
    L1_gr.create_dataset('timeseries', data=L1_data, compression='gzip')
    L1_gr.create_dataset('gps_start', data=L1_gps_start, compression='gzip')
    L1_gr.create_dataset('snr', data=L1_snr, compression='gzip')
        
    # write injection parameters
    inj_group = f.create_group('injection_parameters')
    for key, val in new_injection_parameters.items():
        inj_group.create_dataset(key, data=val, compression='gzip')
    
    # write some attributes
    f.attrs.update(dict(
        fs=params.fs, time_shift=params.time_shift,
        whiten_duration=params.whiten_duration,
        duration=params.duration,
        n_samp=len(H1_data),
    ))
